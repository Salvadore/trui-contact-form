import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appBootstrapErrors]'
})
export class BootstrapErrorsDirective {

  constructor(
    private el: ElementRef,
    private control: NgControl,
    private renderer: Renderer2
  ) { }


  @HostListener('focusout', ['$event']) onFocusout() {
    this.onEvent();
  }

  private onEvent() {
    if (this.showErrors()) {
      this.renderer.addClass(this.el.nativeElement, 'is-invalid');
      this.renderer.setProperty(this.el.nativeElement, "data-bs-toggle", "tooltip");
      this.renderer.setProperty(this.el.nativeElement, "title", this.getErrorDescription());
    } else {
      this.renderer.removeClass(this.el.nativeElement, 'is-invalid');
      this.renderer.removeAttribute(this.el.nativeElement, "data-bs-toggle");
      this.renderer.removeAttribute(this.el.nativeElement, "title");
    }
  }

  private showErrors(): boolean {
    return Boolean((this.control.dirty || this.control.touched) && this.isNotValid());
  }

  private isNotValid(): boolean {
    return Boolean(this.control.invalid);
  }

  private getErrorDescription(): string {
    const errors = this.control.errors;
    let description: string = "Wystąpił błąd!";
    if (errors === null)
      return description;
    else if (errors.required)
      description = "Pole jest wymagane!";
    else if (errors.nipValidator)
      description = "Nip nie jest poprawny!";
    else if (errors.email)
      description = "E-mail nie jest poprawny!";
    return description;
  }
}
