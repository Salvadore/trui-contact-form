import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BootstrapErrorsDirective } from './directives/bootstrap-errors.directive';

const MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
 ];

const DIRECTIVES = [
  BootstrapErrorsDirective
];

@NgModule({
  imports: [
    ...MODULES
  ],
  declarations: [
    ...DIRECTIVES,
  ],
  exports: [
    ...MODULES,
    ...DIRECTIVES,
  ]
})
export class SharedModule { }
