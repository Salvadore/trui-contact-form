import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SelectItem } from '../shared/models/select-item';
import { nipValidator } from '../utils/validators/nip-validator';

enum FormControlNames {
  NAME = 'name',
  NIP = 'nip',
  EMAIL_ADDRESS = 'email',
  TELEPHONE = 'telephone',
  TOPIC = 'topic',
  DRIVING_LICENSE = 'drivingLicense',
  CONTENT = 'content',
  ALL_AGREEMENTS = 'allAgreements',
  DATA_PROCESSING_AGREEMENT = 'dataProcessingAgreement',
  MARKETING_AGREEMENT = 'marketingAgreement'
}

const TopicOptions: Array<SelectItem> = [
  // wartości wyniósłbym do enuma w momencie jak by były określone a całą stałą do oddzielnego pliku
  { label: "Współpraca", value: "COOPERATION" },
  { label: "Inny", value: "OTHER" }
];

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, OnDestroy {

  contactForm: FormGroup;
  controlNames: typeof FormControlNames;
  topicOptions: Array<SelectItem>;
  private subject: Subject<boolean>;

  constructor(private formBuilder: FormBuilder) {
    this.controlNames = FormControlNames;
    this.topicOptions = TopicOptions;
    this.subject = new Subject<boolean>();
    this.contactForm = this.formBuilder.group({
      [FormControlNames.NAME]: ['', [Validators.required]],
      [FormControlNames.NIP]: ['', [Validators.required, nipValidator]],
      [FormControlNames.EMAIL_ADDRESS]: ['', [Validators.required, Validators.email]],
      [FormControlNames.TELEPHONE]: [''],
      [FormControlNames.TOPIC]: [null, [Validators.required]],
      [FormControlNames.DRIVING_LICENSE]: [false],
      [FormControlNames.CONTENT]: [''],
      [FormControlNames.ALL_AGREEMENTS]: [false],
      [FormControlNames.DATA_PROCESSING_AGREEMENT]: [false, [Validators.requiredTrue]],
      [FormControlNames.MARKETING_AGREEMENT]: [false]
    });
  }

  ngOnInit() {
    this.subscribeAgreenetsChanges();
  }

  ngOnDestroy() {
    this.subject.unsubscribe();
  }

  private subscribeAgreenetsChanges() {
    this.contactForm.get(FormControlNames.DATA_PROCESSING_AGREEMENT)?.valueChanges
      .pipe(takeUntil(this.subject))
      .subscribe((value: boolean) => this.agreementChange(FormControlNames.DATA_PROCESSING_AGREEMENT, value));
    this.contactForm.get(FormControlNames.MARKETING_AGREEMENT)?.valueChanges
      .pipe(takeUntil(this.subject))
      .subscribe((value: boolean) => this.agreementChange(FormControlNames.MARKETING_AGREEMENT, value));
  }

  private agreementChange(controlName: FormControlNames, value: boolean) {
    const allAgreementsControl: AbstractControl | null = this.contactForm.get(FormControlNames.ALL_AGREEMENTS);
    const secondAgreementValue: boolean =
      controlName === FormControlNames.DATA_PROCESSING_AGREEMENT ?
        this.contactForm.get(FormControlNames.MARKETING_AGREEMENT)?.value :
        this.contactForm.get(FormControlNames.DATA_PROCESSING_AGREEMENT)?.value;

    if (value === true && secondAgreementValue === true)
      allAgreementsControl?.setValue(true);
    else if (allAgreementsControl?.value === true)
      allAgreementsControl?.setValue(false);
  }

  allAgreementChange() {
    const value: boolean = this.contactForm.get(FormControlNames.ALL_AGREEMENTS)?.value;
    const dataProcessingAgreementControl: AbstractControl | null = this.contactForm.get(FormControlNames.DATA_PROCESSING_AGREEMENT);
    const marketingAgreementControl: AbstractControl | null = this.contactForm.get(FormControlNames.MARKETING_AGREEMENT);

    if (dataProcessingAgreementControl?.value !== value)
      dataProcessingAgreementControl?.setValue(value);
    if (marketingAgreementControl?.value !== value)
      marketingAgreementControl?.setValue(value);
  }

  onFormSubmit() {
    console.log(this.contactForm.value);
  }
}

