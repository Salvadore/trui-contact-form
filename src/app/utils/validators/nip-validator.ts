import { AbstractControl } from '@angular/forms';

export function nipValidator(abstractControl: AbstractControl) {
    let nip = abstractControl.value;
    var weights = [6, 5, 7, 2, 3, 4, 5, 6, 7];
    nip = nip.replace(/[\s-]/g, '');
    if (nip.length === 10 && parseInt(nip, 10) > 0) {
        var sum = 0;
        for (var i = 0; i < 9; i++) {
            sum += Number(nip[i]) * weights[i];
        }
        if ((sum % 11) === Number(nip[9]))
            return null;
    }
    return { nipValidator: true };
}
